
import { quanLyDatVeServ } from "../../Services/BookticketsManage";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { DETAIL_MOVIE } from "../Constants/ManageTheatre";
import { DAT_VE, LAY_CHI_TIET_PHONG_VE } from "../Constants/QuanLiDatVe";
import { setLoadingOffAction, setLoadingOnAction } from "./ActionSpinner";


export const quanLiDatVeAction = (maLichChieu) => {
    return async dispatch => {
        try {
            dispatch(setLoadingOnAction)
            const result = await quanLyDatVeServ.layChiTietPhongVe(maLichChieu);
            dispatch({
                type: LAY_CHI_TIET_PHONG_VE,
                chiTietPhongVe: result.data.content
            })
            dispatch(setLoadingOffAction)
        } catch (err) {
            dispatch(setLoadingOffAction)
        }
    }
}

export const datVeAction = (thongTinDatVe = new ThongTinDatVe) => {
    return async dispatch => {
        try {
            dispatch(setLoadingOnAction)
            const result = await quanLyDatVeServ.datVe(thongTinDatVe);
            dispatch(setLoadingOffAction)
        } catch (err) {
            dispatch(setLoadingOffAction)
        }
    }
}