import { movieServ } from "../../Services/MovieService";
import { INFO_FILMS, LAY_DANH_SACH_PHIM, SET_THONG_TIN_PHIM } from "../Constants/ManageFilms";


export const GetListMovie = () => {
    return async dispatch => {
        try {
            const result = await movieServ.getListMovie();
            dispatch({
                type: LAY_DANH_SACH_PHIM,
                arrFilms: result.data.content
            })
        } catch (err) {
        }
    }
}
export const addFilmsUploadFilms = (formData) => {
    return async dispatch => {
        try {
            const result = await movieServ.addFilmsUploadFilms(formData);
            alert('Thêm Phim Thành Công')
        } catch (err) {
            alert('Thêm Phim Thất Bại')
        }
    }
}
export const getInfoFilmsAction = (maPhim) => {
    return async dispatch => {
        try {
            const result = await movieServ.getMovieInfo(maPhim);

            dispatch({
                type: INFO_FILMS,
                infoFilms: result.data.content
            })
        } catch (err) {

        }
    }
}
export const getInforMovieAction = (id) => {
    return async (dispatch) => {
        try {
            const result = await movieServ.getMovieInfo(id);

            dispatch({
                type: SET_THONG_TIN_PHIM,
                infoFilms: result.data.content,
            });
        } catch (err) {
        }
    };
};

export const capNhatPhimUploadAction = (formData) => {
    return async (dispatch) => {
        try {
            const result = await movieServ.capNhatPhimUpload(formData);
            alert("Cập nhật phim thành công");
            dispatch(GetListMovie());
        } catch (err) {
        }
    };
};
