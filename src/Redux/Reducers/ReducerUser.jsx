import { localServ } from "../../Services/LocalService"
import { SET_INFO_USER, SET_USER_LOGIN, SET_USER_SIGNUP } from "../Constants/ConstantsUser"


const initialState = {
    userInfor: localServ.user.get(),
    thongTinNguoiDung: {}
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_LOGIN: {
            state.userInfor = action.payload
            return { ...state };
        }
        case SET_USER_SIGNUP: {
            state.userInfor = action.payload
            return { ...state };
        }
        case SET_INFO_USER: {
            state.thongTinNguoiDung = action.thongTinNguoiDung;
            return { ...state };
        }
        default: return state
    }
}
export default userReducer;