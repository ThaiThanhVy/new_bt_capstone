import { DAT_VE, LAY_CHI_TIET_PHONG_VE } from "../Constants/QuanLiDatVe"

export const initialState = {
    chiTietPhongVe: {},
    danhSachGheDangDat: []
}

const quanLiDatVeReducer = (state = initialState, action) => {
    switch (action.type) {
        case LAY_CHI_TIET_PHONG_VE: {
            state.chiTietPhongVe = action.chiTietPhongVe
            return { ...state }
        }
        case DAT_VE: {
            let danhSachGheDangDatUpdate = [...state.danhSachGheDangDat]

            let index = danhSachGheDangDatUpdate.findIndex(gheDangDat => gheDangDat.maGhe === action.ghe.maGhe)
            if (index !== -1) {
                danhSachGheDangDatUpdate.splice(index, 1)
            } else {
                danhSachGheDangDatUpdate.push(action.ghe)
            }

            return { ...state, danhSachGheDangDat: danhSachGheDangDatUpdate }
        }

        default: return { ...state }
    }
}

export default quanLiDatVeReducer