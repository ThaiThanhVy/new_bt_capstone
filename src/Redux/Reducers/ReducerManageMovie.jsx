import { INFO_FILMS, LAY_DANH_SACH_PHIM, SET_PHIM_DANG_CHIEU, SET_PHIM_SAP_CHIEU } from "../Constants/ManageFilms"
import { DETAIL_MOVIE } from "../Constants/ManageTheatre"



const stateDefault = {
    arrFilms: [],
    detailMovie: {},
    arrFilmDetail: [],
    infoFilms: {},
    dangChieu: true,
    sapChieu: false,
    arrFilmDefaut: []
}

const reducerManageMovie = (state = stateDefault, action) => {
    switch (action.type) {
        case LAY_DANH_SACH_PHIM: {
            state.arrFilms = action.arrFilms
            state.arrFilmDetail = state.arrFilms
            state.arrFilmDefaut = state.arrFilms
            return { ...state }
        }

        case SET_PHIM_DANG_CHIEU: {
            state.dangChieu = !state.dangChieu
            state.arrFilms = state.arrFilmDefaut.filter(film => film.dangChieu === state.dangChieu)
            return { ...state }
        }
        case SET_PHIM_SAP_CHIEU: {
            state.sapChieu = !state.sapChieu
            state.arrFilms = state.arrFilmDefaut.filter(film => film.sapChieu === state.sapChieu)
            return { ...state }
        }

        case DETAIL_MOVIE: {
            state.detailMovie = action.detailMovie

            return { ...state }
        }

        case INFO_FILMS: {
            state.infoFilms = action.infoFilms
            return { ...state }
        }

        default: return { ...state }
    }
}

export default reducerManageMovie; 