import axios from "axios"
import { BASE_URL, GROUPID, https, TOKEN_CYBERSOFT } from "./ConfigURL"

export const movieServ = {
    getListMovie: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`,
            method: "GET",
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            }
        });
    },
    getDataMovieBytheater: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUPID}`,
            method: "GET",
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            }
        })
    },

    getDataCarousel: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
            method: "GET",
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            }
        })
    },

    addFilmsUploadFilms: (formData) => {
        return https.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, formData)
    },

    getMovieInfo: (maPhim) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
    },
    capNhatPhimUpload: (formData) => {
        return https.post(`/api/QuanLyPhim/CapNhatPhimUpload`, formData);
    },
};