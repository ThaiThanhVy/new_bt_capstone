import axios from "axios"
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./ConfigURL"

export const movieServ = {
    postLogin: (data) => {
        let uri = `/api/QuanLyNguoiDung/DangNhap`
        return https.post(uri, data)
    },
    postSignup: (data) => {
        let uri = `/api/QuanLyNguoiDung/DangKy`
        return https.post(uri, data)
    },
    getInfoBookingResults: () => {
        let uri = `/api/QuanLyNguoiDung/ThongTinTaiKhoan`
        return https.post(uri)
    }
};