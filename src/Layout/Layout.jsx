import React from 'react'
import Footer from '../components/HeaderTheme/Footer'
import HeaderTheme from '../components/HeaderTheme/HeaderTheme'
import HomeCarousel from '../components/HeaderTheme/HomeCarousel'
import HomePage from '../Page/HomePage/HomePage'

export default function Layout(props) {
    let { Component } = props
    return (
        <div>
            <HeaderTheme />
            <div><Component /></div>
            <Footer />
        </div>
    )
}
