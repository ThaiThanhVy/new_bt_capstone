import {
    DesktopOutlined,
    FileOutlined,
    PieChartOutlined,
    TeamOutlined,
    UserOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu } from "antd";
import React, { Component, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { localServ, USER } from "../../../Services/LocalService";
// import Films from "../Films/Films";
import '../../../assets/styles/Checkout.css'
import SubMenu from "antd/lib/menu/SubMenu";

const { Header, Content, Footer, Sider } = Layout;


function getItem(label, key, icon, children) {
    return {
        key,
        icon,
        children,
        label,
    };
}


const DashBoard = (props) => {
    let { Component } = props
    // let navigate = useNavigate();
    let user = useSelector((state) => {
        return state.userReducer.userInfor;
    });
    const [collapsed, setCollapsed] = useState(false);

    if (!localStorage.getItem(USER)) {
        alert("Bạn Không có quyền truy Cập vào trang này");
        return (window.location.href = "/");
    }

    if (user.maLoaiNguoiDung !== "QuanTri") {
        alert('Chỉ có tài khoản quản trị mới vô được trang này!')
        return (window.location.href = "/");
    }

    let handleRemove = () => {
        localServ.user.remove();
        setTimeout(() => {
            window.location.href = "/";
        }, 1000);
    };

    let opaerations = () => {
        if (user) {
            return (
                <div className="items-center flex-shrink-0 hidden lg:flex mt-1" style={{ paddingLeft: 1270 }}>
                    <button style={{ width: 52, height: 52, backgroundColor: 'rgba(191, 62, 129, 0.8)' }} className=' mr-3 font-medium rounded-full opacity-100 text-white'>{user.hoTen.substr(0, 1)}</button>
                    <button onClick={() => { handleRemove() }} className='self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 text-white'>
                        Đăng Xuất
                    </button>
                </div>
            );
        }
    };
    return (
        <Layout
            style={{
                minHeight: '100vh',
            }}
        >
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div className=" bg-opacity-40 flex justify-between h-16 mx-auto">
                    <div>
                        <a rel="noopener noreferrer" href="#" aria-label="Back to homepage" className="flex items-center p-2 animate-bounce">
                            <NavLink to={'/'}><h3 className="w-8 h-8 text-violet-400 text-3xl mt-3 ml-3">CyberMovie</h3></NavLink>
                        </a>
                    </div>
                    <div>
                        <ul>
                            <li className="flex text-right">{opaerations()}</li>
                        </ul>
                    </div>
                </div>
                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                    <Menu.Item key='1' icon={<UserOutlined />}>
                        <NavLink to={"/admin/user"}>User</NavLink>
                    </Menu.Item>
                    <SubMenu key='sub1' icon={<FileOutlined />} title='Films'>
                        <Menu.Item key='2' icon={<FileOutlined />}>
                            <NavLink to={"/admin/film"}>Films</NavLink>

                        </Menu.Item>
                        <Menu.Item key='3' icon={<FileOutlined />}>
                            <NavLink to={"/admin/film/addnewfilms"}>Add New Films</NavLink>
                        </Menu.Item>
                    </SubMenu>
                    <Menu.Item key='4' icon={<DesktopOutlined />}>
                        <NavLink to={"/admin/showtime"}>ShowTime</NavLink>
                    </Menu.Item>
                </Menu>
            </Sider>
            <Layout className="">
                <Header
                    className=""
                    style={{
                        padding: 0,
                    }}
                />
                <Content
                    style={{
                        margin: '0 16px',
                    }}
                >
                    <Breadcrumb
                        style={{
                            margin: '16px 0',
                        }}
                    >
                        {/* <Breadcrumb.Item>{<Films />}</Breadcrumb.Item> */}
                    </Breadcrumb>
                    <div
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            minHeight: 440,
                        }}
                    >
                        <Component />
                    </div>
                </Content>
            </Layout>
        </Layout >
    );
};

export default DashBoard;
