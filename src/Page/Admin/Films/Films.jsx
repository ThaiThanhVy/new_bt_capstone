import React, { Fragment } from 'react'
import { Table } from 'antd';
import { AudioOutlined, SearchOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Input, } from 'antd';
import { Button } from 'antd/lib/radio';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { GetListMovie } from '../../../Redux/Actions/ManageMoviesAction';
import { NavLink } from 'react-router-dom';


const { Search } = Input;
export default function Films() {

    const { arrFilmDetail } = useSelector(state => state.reducerManageMovie)
    console.log('arrFilmDetail: ', arrFilmDetail);

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(GetListMovie())
    }, [])

    const columns = [
        {
            title: 'Mã Phim',
            dataIndex: 'maPhim',
            defaultSortOrder: 'descend',
            width: '10%',
            sorter: (a, b) => a.maPhim - b.maPhim,
        },
        {
            title: 'Hình Ảnh',
            dataIndex: 'hinhAnh',
            render: (text, films, index) => {
                return <Fragment>
                    <img src={films.hinhAnh} alt={films.hinhAnh} width='50' height='50' onError={(e) => {
                        e.target.onError = null;
                        e.target.src = `https://picsum.photos/id/${index}/50/50`
                    }}></img>
                </Fragment >
            },
            width: '20%',
            defaultSortOrder: 'descend',
            // sorter: (a, b) => a.age - b.age,
        },
        {
            title: 'Tên Phim',
            dataIndex: 'tenPhim',
            defaultSortOrder: 'descend',
            render: (text, films) => {
                return <Fragment>
                    {films.tenPhim.toLowerCase()}
                </Fragment>
            },
            width: '25%'
        },
        {
            title: 'Mô Tả',
            dataIndex: 'moTa',
            defaultSortOrder: 'descend',
            // sorter: (a, b) => {
            //     let moTaPhimA = a.moTa.toLowerCase().trim()
            //     let moTaPhimB = b.moTa.toLowerCase().trim()
            //     if (moTaPhimA > moTaPhimB) {
            //         return 1
            //     } else {
            //         return -1
            //     }
            // }
            render: (text, films) => {
                return <Fragment>
                    {films.moTa.length > 100 ? films.moTa.substr(0, 50) + '...' : films.moTa}
                </Fragment>
            },
            width: '25%'
        },
        {
            title: 'Hành Động',
            dataIndex: 'hanhDong',
            defaultSortOrder: 'descend',
            render: (text, films) => {
                return <Fragment>
                    <NavLink to={`/admin/films/editfilms/${films.maPhim}`} className='mr-2 ml-2 text-2xl' key={1} style={{ color: 'blue' }}><EditOutlined /></NavLink>
                    <NavLink className=' text-2xl' key={2} style={{ color: 'red' }}><DeleteOutlined /></NavLink>
                </Fragment>
            },
            width: '20%'
        },
    ];
    const data = arrFilmDetail


    const onChange = (pagination, filters, sorter, extra) => {
        console.log('params', pagination, filters, sorter, extra);
    };
    const suffix = (
        <AudioOutlined
            style={{
                fontSize: 16,
                color: '#1890ff',
            }}
        />
    );
    const onSearch = (value) => console.log(value);
    return (
        <div className='container'>
            <h3 className='text-4xl'>Quản lí phim</h3>
            <NavLink to='/admin/film/addnewfilms'>
                <Button className='mb-4'>Thêm Phim</Button>
            </NavLink>
            <Search
                className='mb-4 align-middle'
                placeholder="Tìm Kiếm"
                enterButton={<SearchOutlined />}
                size="large"
                onSearch={onSearch}
            />
            <Table columns={columns} dataSource={data} onChange={onChange} />;
        </div>
    )
}
