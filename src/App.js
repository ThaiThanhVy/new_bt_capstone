import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css';
import Layout from './Layout/Layout';
// npm i ant design
import "antd/dist/antd.css";
// row slick 
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import DetailPage from './Page/DetailPage/DetailPage';
import HomePage from './Page/HomePage/HomePage';
import LoginPage from './Page/LoginPage/LoginPage';
import Contact from './Page/Contact/Contact';
import Spinner from './components/Spinner/Spinner';
import Checkout from './Page/Checkout/Checkout';
import DashBoard from './Page/Admin/DashBoard/DashBoard';
import Films from './Page/Admin/Films/Films';
import User from './Page/Admin/User/User';
import ShowTime from './Page/Admin/ShowTime/ShowTime';
import AddNewFilms from './Page/Admin/Films/AddNewFilms/AddNewFilms';
import EditFilms from './Page/Admin/Films/EditFilms/EditFilms';
import SignupPage from './Page/SignupPage/SignupPage';
import BookingResults from './Page/Checkout/BookingResults';
import LayoutHeader from './Layout/LayoutHeader';
function App() {


  return (
    <div>
      <Spinner />
      {/* BrowserRouter Giống như Provider có cái này thì Routes mới hoạt động */}
      <BrowserRouter>
        {/* Phân Trang */}
        <Routes>
          <Route
            path="/"
            element={
              <Layout Component={HomePage} />
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/signup" element={<SignupPage />} />
          <Route
            path="/detail/:id"
            element={<LayoutHeader Component={DetailPage} />}
          />
          <Route path="/contact" element={<Contact />} />
          <Route path="/checkout/:id" element={<LayoutHeader Component={Checkout} />} />
          <Route path="/checkout/bookingresults" element={<LayoutHeader Component={BookingResults} />} />
          {/* <Route path="/admin/dashboard" element={<DashBoard Component={DashBoard} />} /> */}
          <Route path="/admin/user" element={<DashBoard Component={User} />} />
          <Route path="/admin/film" element={<DashBoard Component={Films} />} />
          <Route path="/admin/film/addnewfilms" element={<DashBoard Component={AddNewFilms} />} />
          <Route path="/admin/film/editfimls/:id" element={<DashBoard Component={EditFilms} />} />
          <Route path="/admin/showtime" element={<DashBoard Component={ShowTime} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;